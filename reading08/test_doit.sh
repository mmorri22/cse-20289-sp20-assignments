#!/bin/bash

SCRIPT=doit.py
WORKSPACE=/tmp/$SCRIPT.$(id -u)
FAILURES=0

export LC_ALL=C

error() {
    echo "$@"
    echo
    [ -r $WORKSPACE/test ] && cat $WORKSPACE/test
    echo
    FAILURES=$((FAILURES + 1))
}

cleanup() {
    STATUS=${1:-$FAILURES}
    rm -fr $WORKSPACE
    exit $STATUS
}

mkdir $WORKSPACE

trap "cleanup" EXIT
trap "cleanup 1" INT TERM

echo "Testing $SCRIPT ..."

printf " %-40s ... " "$SCRIPT"
./$SCRIPT &> $WORKSPACE/test
if [ $? -ne 2 ]; then
    error "Failure"
else
    echo "Success"
fi

printf " %-40s ... " "$SCRIPT asdf"
./$SCRIPT asdf &> $WORKSPACE/test
if [ $? -ne 2 ]; then
    error "Failure"
else
    echo "Success"
fi

printf " %-40s ... " "$SCRIPT true"
./$SCRIPT true &> $WORKSPACE/test
if [ $? -ne 0 ]; then
    error "Failure"
else
    echo "Success"
fi

printf " %-40s ... " "$SCRIPT false"
./$SCRIPT false &> $WORKSPACE/test
if [ $? -ne 1 ]; then
    error "Failure"
else
    echo "Success"
fi

printf " %-40s ... " "$SCRIPT ls"
diff <(./$SCRIPT ls) <(ls) &> $WORKSPACE/test
if [ $? -ne 0 ]; then
    error "Failure"
else
    echo "Success"
fi

printf " %-40s ... " "$SCRIPT ls -l"
diff <(./$SCRIPT ls -l) <(ls -l) &> $WORKSPACE/test
if [ $? -ne 0 ]; then
    error "Failure"
else
    echo "Success"
fi

printf " %-40s ... " "$SCRIPT find /etc -type f"
diff <(./$SCRIPT find /etc -type f 2> /dev/null) <(find /etc -type f 2> /dev/null) &> $WORKSPACE/test
if [ $? -ne 0 ]; then
    error "Failure"
else
    echo "Success"
fi

printf " %-40s ... " "no system or subprocess"
grep -E '(system|subprocess)' $SCRIPT &> $WORKSPACE/test
if [ $? -eq 0 ]; then
    error "Failure"
else
    echo "Success"
fi

TESTS=$(($(grep -c Success $0) - 1))
echo "   Score $(echo "scale=2; ($TESTS - $FAILURES) / $TESTS.0 * 1.5" | bc | awk '{printf "%0.2f\n", $1}')"
echo
